﻿# easy-fast-jdbc

一个支持高并发的jdbc链接池，体积小、速度快，目前只支持mysql<br>

### 特色
* session接口为永久会话，可以一直使用
* user_id会自动映射成userId，无需as别名
* 执行查询后会根据sql和对象自动记录映射的字段，无需重新解析字段，查询更快
* 根据project.entities配置，自动装载Entity对象
* 可多个不同数据库jdbc同时使用，互不影响
* 访问高峰时自动增加连接数，低频时再自动释放
* 当有连接release时，如果存在已失效的连接，框架会自动补充到初始化连接数
* 日志详细记录返回的行数，列数及返回的时间和部分执行结果
* 支持分页查询，返回PageInfo
* 支持简单的SQL构造

### 运行环境
* JDK(1.7+)
* mysql-connector-java(5.1.25+)

### ChangeLog
<a href='https://gitee.com/yydf/easy-jdbc/releases'>点击查看更新日志</a>

### Discussing
* <a href='https://gitee.com/yydf/easy-jdbc/issues/new'>提交issue</a>
* 交流群616698275 答案easy
* email：441430565@qq.com

### WIKI
https://gitee.com/yydf/easy-jdbc/wikis/pages

### DEMO
* 直接使用的demo在test包下
* https://gitee.com/yydf/easystruts-xjcy/blob/master/demo.zip	[Dao层]

### 注解对象生成
下载生成工具，打开exe文件，连接-点击数据表-保存(ctrl+s)或保存全部