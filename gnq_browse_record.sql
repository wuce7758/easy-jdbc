/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : fastadmin

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-08-14 13:39:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gnq_browse_record
-- ----------------------------
DROP TABLE IF EXISTS `gnq_browse_record`;
CREATE TABLE `gnq_browse_record` (
  `browse_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `target_type` tinyint(4) NOT NULL DEFAULT '0',
  `browse_num` int(11) NOT NULL DEFAULT '0',
  `browse_time` datetime NOT NULL,
  PRIMARY KEY (`browse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
