package com.test;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.geneqiao.jdbc.DefaultSessionImpl;
import com.geneqiao.jdbc.build.ISqlSession;
import com.geneqiao.jdbc.build.ISqlTranction;
import com.geneqiao.jdbc.build.PageInfo;
import com.geneqiao.jdbc.pool.JDBCDataSource;

public class TestBrowser
{
	private static final Logger logger = Logger.getLogger(TestBrowser.class);

	public static void main(String[] args) throws SQLException
	{
		JDBCDataSource dataSource = JDBCDataSource.getInstance();
		ISqlSession session = DefaultSessionImpl.getInstence(dataSource);
		List<String> list2 = session.QueryList("select tttt from gnq_browse_record");
		System.out.println(list2.get(0).toString());
		
		// 查询列表
		session.Query(GnqBrowseRecord.class);
		session.Query(GnqBrowseRecord.class, "select * from gnq_browse_record");
		// 查询单条
		session.Single(GnqBrowseRecord.class, "select * from gnq_browse_record where browse_id=?", 1);
		// 查询单列
		List<Integer> list = session.QueryList("select browse_id from gnq_browse_record");
		//List<String> list2 = session.QueryList("select browse_id from gnq_browse_record");
		System.out.println(list.size() + " " + list2.size());
		// 查询单值
		session.getSingle("select browse_id from gnq_browse_record where browse_id=?", 1);
		// 查询键值对
		session.QueryMap("select browse_id,browse_num from gnq_browse_record");
		// 增、删、改
		GnqBrowseRecord gbr = new GnqBrowseRecord();
		gbr.setUserId(1);
		gbr.setTargetId(2);
		gbr.setBrowseNum(1);
		gbr.setBrowseTime(new Date());
		gbr.setTargetType(0);
		session.save(gbr); // 保存对象
		System.out.println("browse_id =========" + gbr.getBrowseId());
		gbr.setBrowseId(gbr.getBrowseId());
		session.update(gbr); // 通过主键更新所有字段
		session.updateNotNull(gbr); // 通过主键更新不为NULL字段
		session.delete(gbr); // 通过所有不为NULL字段删除记录
		
		for (int i = 0; i < 2; i++)
		{
			batchTran(session, gbr);
		}
		// 执行SQL
		session.Execute("delete from gnq_browse_record where browse_id=?", 1);
		session.Query(GnqBrowseRecord.class);
		//分页查询
		PageInfo pageInfo = session.queryByPage(GnqBrowseRecord.class, 1, 15, "select * from gnq_browse_record");
		System.out.println(pageInfo);
		dataSource.shutdown();
	}

	private static void batchTran(ISqlSession session, GnqBrowseRecord gbr)
	{
		// 批量事务处理
		ISqlTranction tran = null;
		try
		{
			// 获取事务连接
			tran = session.beginTranction();
			Object obj = session.getSingle(tran, "select browse_id from gnq_browse_record where browse_id=?", gbr.getBrowseId());
			GnqBrowseRecord gbr2 = new GnqBrowseRecord();
			gbr2.setBrowseId(Integer.valueOf(obj.toString()));
			session.delete(tran, gbr2);
			session.save(tran, gbr);
			System.out.println("browse_id =========" + gbr.getBrowseId());
			tran.commit();// 最后事务必须提交，否则会产生未知错误
		}
		catch (SQLException e)
		{
			// 回滚
			tran.rollback();
			logger.error("执行事务失败", e);
		}
		finally
		{
			// 必须关闭事务
			session.close(tran);
		}
	}
}
