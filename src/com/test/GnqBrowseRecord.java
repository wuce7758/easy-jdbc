﻿package com.test;

import com.geneqiao.jdbc.jpa.Column;
import com.geneqiao.jdbc.jpa.Entity;
import com.geneqiao.jdbc.jpa.GeneratedValue;
import com.geneqiao.jdbc.jpa.GenerationType;
import com.geneqiao.jdbc.jpa.Id;
import com.geneqiao.jdbc.jpa.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zml
 *
 * 2016年09月29日 14:52:23
 */
@Entity
@Table(name = "GNQ_BROWSE_RECORD")
public class GnqBrowseRecord implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

	/**
     *浏览记录自增ID
	 */
	private Integer browseId;

	/**
     *用户ID
	 */
	private Integer userId;

	/**
     *来源ID
	 */
	private Integer targetId;

	/**
     *来源类型1话题2课程
	 */
	private Integer targetType;

	/**
     *浏览次数
	 */
	private Integer browseNum;

	/**
     *浏览时间
	 */
	private Date browseTime;


	/**
     *浏览记录自增ID
     */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BROWSE_ID", nullable=false)
	public Integer getBrowseId() {
		return browseId;
	}

	public void setBrowseId(Integer browseId) {
		this.browseId = browseId;
	}

	/**
     *用户ID
     */
	@Column(name="USER_ID", nullable=false)
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
     *来源ID
     */
	@Column(name="TARGET_ID", nullable=false)
	public Integer getTargetId() {
		return targetId;
	}

	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}

	/**
     *来源类型1话题2课程
     */
	@Column(name="TARGET_TYPE", nullable=false)
	public Integer getTargetType() {
		return targetType;
	}

	public void setTargetType(Integer targetType) {
		this.targetType = targetType;
	}

	/**
     *浏览次数
     */
	@Column(name="BROWSE_NUM", nullable=false)
	public Integer getBrowseNum() {
		return browseNum;
	}

	public void setBrowseNum(Integer browseNum) {
		this.browseNum = browseNum;
	}

	/**
     *浏览时间
     */
	@Column(name="BROWSE_TIME", nullable=false)
	public Date getBrowseTime() {
		return browseTime;
	}

	public void setBrowseTime(Date browseTime) {
		this.browseTime = browseTime;
	}

}
