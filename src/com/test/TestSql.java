package com.test;

import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

import com.geneqiao.jdbc.DefaultSessionImpl;
import com.geneqiao.jdbc.build.ISqlSession;
import com.geneqiao.jdbc.build.ISqlTranction;
import com.geneqiao.jdbc.pool.JDBCDataSource;
import com.geneqiao.jdbc.sql.Sql;

public class TestSql
{
	@Test
	public void test()
	{
		System.out.println(Sql.count("test"));
		System.out.println(Sql.select("test", "a").inner("test1", "zz").inner("test2", "xx").where("a", "b"));
		System.out.println(Sql.select("test", "a").left("test1", "zz").where("a", "b"));
		System.out.println(Sql.update("test", "a", "b").where("c"));
		System.out.println(Sql.delete("test").where("c"));
		System.out.println(Sql.insert("test", "a", "b"));
	}

	static long start;
	static int num = 0;

	public static void main(String[] args)
	{
		JDBCDataSource dataSource = JDBCDataSource.getInstance();
		ISqlSession session = DefaultSessionImpl.getInstence(dataSource);
		ExecutorService es = Executors.newCachedThreadPool();
		start = System.currentTimeMillis();
		if (session.Execute("delete from test"))
		{
			for (int i = 0; i < 5000; i++)
			{
				es.execute(new Factory(session));
			}
		}
		es.shutdown();
		dataSource.shutdown();
	}
}

class Factory implements Runnable
{
	private ISqlSession pool;

	public Factory(ISqlSession pool)
	{
		this.pool = pool;
	}

	@Override
	public void run()
	{
		ISqlTranction tran = null;
		try
		{
			tran = this.pool.beginTranction();
			this.pool.Execute(tran, "INSERT INTO `test` (`test`, `addtime`) VALUES (?, NOW())",
					Thread.currentThread().getName());
			this.pool.getSingle(tran, Sql.count("gnq_browse_record").toString());
			this.pool.Query(tran, GnqBrowseRecord.class);
			tran.commit();
		}
		catch (SQLException e)
		{
			tran.rollback();
		}
		finally
		{
			this.pool.close(tran);
		}
		TestSql.num++;
		if (TestSql.num == 5000)
		{
			System.out.println("success ================= " + (System.currentTimeMillis() - TestSql.start));
		}
	}
}
