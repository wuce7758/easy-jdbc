package com.geneqiao.jdbc.jpa;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.geneqiao.jdbc.util.DataUtils;

public class TableStruct
{

	private String tableName;
	private String primaryKey;
	private String generateKey;
	private Map<String, Method> columnMethods;
	Map<String, Object> columns;
	Map<Integer, Object> buildMap;

	public Map<Integer, Object> getBuildMap()
	{
		return buildMap;
	}

	public void setBuildMap(Map<Integer, Object> buildMap)
	{
		this.buildMap = buildMap;
	}

	public TableStruct(String tblName)
	{
		this.tableName = tblName;
	}

	public String getTableName()
	{
		return tableName;
	}

	public void setTableName(String tableName)
	{
		this.tableName = tableName;
	}

	public String getPrimaryKey()
	{
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey)
	{
		this.primaryKey = primaryKey;
	}

	public boolean hasPrimaryKey()
	{
		return primaryKey != null;
	}

	public boolean hasGenerageKey()
	{
		return generateKey != null;
	}

	public Map<String, Object> getColumns()
	{
		return columns;
	}

	public void setColumns(Map<String, Object> columns)
	{
		this.columns = columns;
	}

	public String getGenerateKey()
	{
		return generateKey;
	}

	public void setGenerateKey(String generateKey)
	{
		this.generateKey = generateKey;
	}
	
	public Map<String, Method> getColumnMethods()
	{
		return columnMethods;
	}

	public void setColumnMethods(Map<String, Method> columnMethods)
	{
		this.columnMethods = columnMethods;
	}

	public void setColumns(Object obj)
	{
		if (this.columnMethods == null || this.columnMethods.isEmpty())
			return;
		columns = new HashMap<>();
		Set<String> columnNames = this.columnMethods.keySet();
		for (String colName : columnNames)
		{
			columns.put(colName, DataUtils.getValue(obj, this.columnMethods.get(colName)));
		}
	}
}
