package com.geneqiao.jdbc.jpa;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
public @interface Column
{
	String name() default "";
	
	boolean nullable() default true;
	
	int length() default 255;
}
