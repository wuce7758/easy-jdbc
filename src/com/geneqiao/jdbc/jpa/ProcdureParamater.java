package com.geneqiao.jdbc.jpa;

import java.sql.Types;

import com.geneqiao.jdbc.util.ParameterType;

public class ProcdureParamater
{

	Integer sort;
	Object value;
	ParameterType parameterType;
	Types sqlTypes;

	public ProcdureParamater(Integer _sort, Object _value)
	{
		this.sort = _sort;
		this.value = _value;
		this.parameterType = ParameterType.IN;
	}

	public ProcdureParamater(Integer _sort)
	{
		this.sort = _sort;
		this.parameterType = ParameterType.OUT;
	}

	public ProcdureParamater(Integer _sort, Object _value, ParameterType _parameterType)
	{
		this.sort = _sort;
		this.value = _value;
		this.parameterType = _parameterType;
	}

	public Integer getSort()
	{
		return sort;
	}

	public Object getValue()
	{
		return value;
	}

	public Integer getInt()
	{
		if (value == null)
		{
			return -1;
		}
		return Integer.valueOf(value.toString());
	}

	public ParameterType getParameterType()
	{
		return parameterType;
	}

	public Types getSqlTypes()
	{
		return sqlTypes;
	}

	public void setValue(Object value)
	{
		this.value = value;
	}
}
