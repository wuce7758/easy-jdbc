package com.geneqiao.jdbc.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class SelectConn
{
	private static final Logger logger = Logger.getLogger(SelectConn.class);

	private Connection conn;
	private String url;
	private String username;
	private String password;

	private int used;

	public SelectConn(Connection conn, String url, String username, String password)
	{
		this.used = 0;
		this.conn = conn;
		this.url = url;
		this.username = username;
		this.password = password;
	}

	public synchronized Connection getConnection()
	{
		try
		{
			if (used > 1024)
			{
				this.conn = DriverManager.getConnection(url, username, password);
				logger.debug("使用次数达到1024次，连接重置");
				used = 1;
				return this.conn;
			}
			if (this.conn.isValid(0))
			{
				used++;
				return this.conn;
			}
			this.conn = DriverManager.getConnection(url, username, password);
			used = 1;
			return this.conn;
		}
		catch (SQLException e)
		{
			logger.error("没有获取到查询连接", e);
			return null;
		}
	}

}
