package com.geneqiao.jdbc.pool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.geneqiao.jdbc.util.ConfigUtils;

public class JDBCDataSource
{

	// 日志
	private static final Logger logger = Logger.getLogger(JDBCDataSource.class);
	private static final String defaultConfig = "jdbc.properties";
	private JDBCPool jdbcPool;

	private String driverClassName;
	private String url;
	private String username;
	private String password;
	private String entities;
	private int maxIdle;
	private int maxActive;
	private boolean withSpring;
	private String configFile;

	public JDBCDataSource()
	{
		this.withSpring = true;
		logger.debug("Building with springframework...");
		getPool();
	}

	private JDBCDataSource(String fileName)
	{
		this.configFile = fileName;
		getPool();
	}

	public static JDBCDataSource getInstance(String fileName)
	{
		return new JDBCDataSource(fileName);
	}

	public static JDBCDataSource getInstance()
	{
		return getInstance(defaultConfig);
	}

	public synchronized Connection getConnection()
	{
		try
		{
			return getPool().getConnection();
		}
		catch (SQLException | InterruptedException e)
		{
			logger.error("获取数据库链接失败", e);
		}
		return null;
	}

	protected JDBCPool getPool()
	{
		if (jdbcPool == null)
		{
			try
			{
				// 如果是spring调用
				if (withSpring)
					jdbcPool = new JDBCPool(getSpringProperties());
				else
					jdbcPool = new JDBCPool(ConfigUtils.getProperties(configFile));
			}
			catch (ClassNotFoundException | SQLException e)
			{
				logger.error("初始化连接池失败", e);
			}
		}
		return jdbcPool;
	}

	public synchronized SelectConn getSelectConnection()
	{
		try
		{
			return getPool().getSelectConnection();
		}
		catch (SQLException e)
		{
			logger.error("获取Select链接失败", e);
		}
		return null;
	}

	private Properties getSpringProperties()
	{
		Properties ps = new Properties();
		ps.put("datasource.driverClassName", driverClassName);
		ps.put("datasource.url", url);
		ps.put("datasource.username", username);
		ps.put("datasource.password", password);
		ps.put("datasource.maxActive", maxActive + "");
		ps.put("datasource.maxIdle", maxIdle + "");
		ps.put("project.entities", entities + "");
		return ps;
	}

	public void releaseConnection(Connection conn, boolean isClose)
	{
		if (conn == null)
			return;
		try
		{
			getPool().releaseConnection(conn, isClose);
		}
		catch (SQLException e)
		{
			logger.error("Release connection faild", e);
		}
	}

	public void releaseConnection(Connection conn)
	{
		releaseConnection(conn, false);
	}

	public void shutdown()
	{
		getPool().shutdown();
	}

	public void close()
	{
		getPool().shutdown();
	}

	public String getDriverClassName()
	{
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName)
	{
		this.driverClassName = driverClassName;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public int getMaxIdle()
	{
		return maxIdle;
	}

	public void setMaxIdle(int maxIdle)
	{
		this.maxIdle = maxIdle;
	}

	public int getMaxActive()
	{
		return maxActive;
	}

	public void setMaxActive(int maxActive)
	{
		this.maxActive = maxActive;
	}

	public String getEntities()
	{
		return entities;
	}

	public void setEntities(String entities)
	{
		this.entities = entities;
	}
}
