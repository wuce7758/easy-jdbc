package com.geneqiao.jdbc.pool;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.geneqiao.jdbc.jpa.TableStruct;

public class JDBCMap
{
	// private static final Logger logger = Logger.getLogger(JDBCMap.class);

	private static final Map<String, Map<Integer, Field>> buildMap = new HashMap<>();
	private static final Map<String, TableStruct> structMap = new HashMap<>();

	public static boolean containsKey(String cacheKey)
	{
		return buildMap.containsKey(cacheKey);
	}

	public static Map<Integer, Field> get(String cacheKey)
	{
		return buildMap.get(cacheKey);
	}

	public static void put(String cacheKey, Map<Integer, Field> map)
	{
		buildMap.put(cacheKey, map);
	}

	public static void addEntity(String name, TableStruct struct)
	{
		structMap.put(name, struct);
	}

	public static boolean findEntity(Class<?> cla)
	{
		return structMap.containsKey(cla.getName());
	}

	public static TableStruct getEntity(Class<?> cla)
	{
		return structMap.get(cla.getName());
	}
}
