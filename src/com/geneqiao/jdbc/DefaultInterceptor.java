package com.geneqiao.jdbc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import com.geneqiao.jdbc.build.ISqlSession;

public class DefaultInterceptor implements InvocationHandler
{

	private Object target;

	public DefaultInterceptor(ISqlSession session)
	{
		this.target = session;
	}

	public void before()
	{
	}

	public void after()
	{
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		before();
		Object result = method.invoke(target, args);
		after();
		return result;
	}
}
