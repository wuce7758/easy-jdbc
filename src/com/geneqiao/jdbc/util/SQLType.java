package com.geneqiao.jdbc.util;

public enum SQLType
{
	INSERT, UPDATE, UPDATENOTNULL, DELETE, SELECT
}
