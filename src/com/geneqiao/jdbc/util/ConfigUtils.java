package com.geneqiao.jdbc.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigUtils
{
	private static final Logger logger = Logger.getLogger(ConfigUtils.class);

	public static final String URL = "datasource.url";
	public static final String USERNAME = "datasource.username";
	public static final String PASSWORD = "datasource.password";

	public static final String INC = "datasource.maxIdle";
	public static final String MAX = "datasource.maxActive";
	public static final String MIN = "datasource.maxIdle";

	public static Properties getProperties(String fileName)
	{
		InputStream is = ConfigUtils.class.getClassLoader().getResourceAsStream(fileName);
		if (is != null)
		{
			Properties ps = new Properties();
			try
			{
				ps.load(is);
				return ps;
			}
			catch (IOException e)
			{
				logger.error("加载‘" + fileName + "’配置文件失败", e);
			}
		}
		return null;
	}
}
