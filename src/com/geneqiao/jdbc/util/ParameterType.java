package com.geneqiao.jdbc.util;

public enum ParameterType
{
	IN, OUT, INOUT
}
