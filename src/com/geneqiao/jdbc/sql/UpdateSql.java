package com.geneqiao.jdbc.sql;

import java.util.ArrayList;
import java.util.List;

public class UpdateSql extends Sql
{
	private static final String SQL_UPDATE = "UPDATE %s t0";
	private List<String> updateColumns = new ArrayList<>();

	public UpdateSql(String table, String[] columns)
	{
		this.tables.add(table);
		for (String str : columns)
		{
			this.updateColumns.add(str);
		}
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(SQL_UPDATE, this.tables.get(0)));
		sb.append(" SET");
		if (updateColumns.size() > 0)
		{
			for (String col : updateColumns)
			{
				sb.append(" t0.").append(col).append("=?").append(",");
			}
			sb.delete(sb.length() - 1, sb.length());
		}
		appendWhere(sb);
		return sb.toString();
	}

	public UpdateSql set(String column)
	{
		return this;
	}
}
