package com.geneqiao.jdbc.sql;

public class CountSql extends Sql
{
	private static final String SQL_COUNT = "SELECT COUNT(1) FROM %s t0";

	public CountSql(String table)
	{
		this.tables.add(table);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(SQL_COUNT , this.tables.get(0)));
		appendWhere(sb);
		return sb.toString();
	}
}
