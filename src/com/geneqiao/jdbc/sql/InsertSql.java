package com.geneqiao.jdbc.sql;

import java.util.ArrayList;
import java.util.List;

public class InsertSql
{
	private static final String SQL_INSERT = "INSERT INTO %s";
	private List<String> insertColumns = new ArrayList<>();
	private String table;

	public InsertSql(String table, String[] columns)
	{
		this.table = table;
		for (String str : columns)
		{
			this.insertColumns.add(str);
		}
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(SQL_INSERT, table));
		if (insertColumns.size() > 0)
		{
			sb.append("(");
			String values = "";
			for (int i = 0; i < insertColumns.size(); i++)
			{
				sb.append(insertColumns.get(i));
				values += "?";
				if (i != insertColumns.size() - 1)
				{
					sb.append(",");
					values += ",";
				}
			}
			sb.append(") VALUES (").append(values).append(")");
		}
		return sb.toString();
	}
}
