package com.geneqiao.jdbc.sql;

import java.util.ArrayList;
import java.util.List;

public abstract class Sql
{
	protected List<String> tables = new ArrayList<>();
	protected List<String> wheres = new ArrayList<>();
	
	public static SelectSql select(String table)
	{
		return new SelectSql(table);
	}
	
	public static SelectSql select(String table, String... columns)
	{
		return new SelectSql(table, columns);
	}

	public static CountSql count(String table)
	{
		return new CountSql(table);
	}

	public static UpdateSql update(String table, String... columns)
	{
		return new UpdateSql(table, columns);
	}

	public static Sql delete(String table)
	{
		return new DeleteSql(table);
	}

	public static InsertSql insert(String table, String... columns)
	{
		return new InsertSql(table, columns);
	}
	
	public Sql where(String... columns)
	{
		for (String str : columns)
		{
			this.wheres.add(str);
		}
		return this;
	}

	protected void appendWhere(StringBuilder sb)
	{
		if(wheres.size() > 0)
		{
			if(sb.toString().contains("WHERE"))
				sb.append(" AND");
			else
				sb.append(" WHERE");
			for (String str : wheres)
			{
				sb.append(" t0.").append(str).append("=?").append(" AND");
			}
			sb.delete(sb.length() - 4, sb.length());
		}
	}
}
