package com.geneqiao.jdbc.sql;

public class DeleteSql extends Sql
{
	private static final String SQL_DELETE = "DELETE FROM %s t0";
	
	public DeleteSql(String table)
	{
		this.tables.add(table);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(SQL_DELETE , this.tables.get(0)));
		appendWhere(sb);
		return sb.toString();
	}
}