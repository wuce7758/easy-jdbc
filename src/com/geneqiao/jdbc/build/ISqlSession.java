package com.geneqiao.jdbc.build;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.geneqiao.jdbc.jpa.ProcdureParamater;

public interface ISqlSession
{
	ISqlTranction beginTranction() throws SQLException;
	
	void close(ISqlTranction tran);
	
	<T> PageInfo queryByPage(Class<T> t, int pageNum, int pageSize, String sql);
	
	<T> PageInfo queryByPage(Class<T> t, int pageNum, int pageSize, String sql, Object... objects);
	
	boolean RunProcdure(String sql);
	
	boolean RunProcdure(String sql, List<ProcdureParamater> paras);
	
	<T> List<T> Query(Class<T> t) throws SQLException;
	
	<T> List<T> Query(Class<T> t, String sql);
	
	<T> List<T> Query(Class<T> t, String sql, Object... objects);
	
	<T> List<T> Query(ISqlTranction tran, Class<T> t) throws SQLException;
	
	<T> List<T> Query(ISqlTranction tran, Class<T> t, String sql) throws SQLException;
	
	<T> List<T> Query(ISqlTranction tran, Class<T> t, String sql, Object... objects) throws SQLException;
	
	<T> List<T> QueryList(String sql);
	
	<T> List<T> QueryList(String sql, Object... objects);
	
	<T> List<T> QueryList(ISqlTranction tran, String sql) throws SQLException;
	
	<T> List<T> QueryList(ISqlTranction tran, String sql, Object... objects) throws SQLException;
	
	<T> T Single(Class<T> t, String sql);
	
	<T> T Single(Class<T> t, String sql, Object... objects);
	
	<T> T Single(ISqlTranction tran, Class<T> t, String sql) throws SQLException;
	
	<T> T Single(ISqlTranction tran, Class<T> t, String sql, Object... objects) throws SQLException;
	
	Map<String, Object> QueryMap(String sql);
	
	Map<String, Object> QueryMap(String sql, Object... objects);
	
	Map<String, Object> QueryMap(ISqlTranction tran, String sql) throws SQLException;
	
	Map<String, Object> QueryMap(ISqlTranction tran, String sql, Object... objects) throws SQLException;
	
	Object getSingle(String sql);
	
	Object getSingle(String sql, Object... objects);
	
	Object getSingle(ISqlTranction tran, String sql) throws SQLException;
	
	Object getSingle(ISqlTranction tran, String sql, Object... objects) throws SQLException;
	
	boolean Execute(String sql);
	
	boolean Execute(String sql, Object... objects);
	
	boolean Execute(ISqlTranction tran, String sql) throws SQLException;
	
	boolean Execute(ISqlTranction tran, String sql, Object... objects) throws SQLException;
	
	boolean Execute(List<String> sqlList);
	
	boolean update(Object obj);
	
	boolean update(ISqlTranction tran, Object obj) throws SQLException;
	
	boolean updateNotNull(Object obj);
	
	boolean updateNotNull(ISqlTranction tran, Object obj) throws SQLException;
	
	boolean save(Object obj);
	
	boolean save(ISqlTranction tran, Object obj) throws SQLException;
	
	boolean saveOrUpdate(Object obj);
	
	boolean saveOrUpdate(ISqlTranction tran, Object obj) throws SQLException;
	
	boolean delete(Object obj);
	
	boolean delete(ISqlTranction tran, Object obj) throws SQLException;
}