package com.geneqiao.jdbc.build;

import java.sql.Connection;

public interface ISqlTranction
{
	Connection Connection();
	
	void commit();

	void rollback();
}
